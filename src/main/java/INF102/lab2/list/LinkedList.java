package INF102.lab2.list;

public class LinkedList<T> implements List<T> {

	private int n;
	
	/**
	 * If list is empty, head == null
	 * else head is the first element of the list.
	 */
	private Node<T> head;

	@Override
	public int size() {
		return n;
	}

	@Override
	public boolean isEmpty() {
		return n == 0;
	}

	@Override
	public T get(int index) {
		return getNode(index).data;
	}

	/**
     * Returns the node at the specified position in this list.
     *
     * @param index index of the node to return
     * @return the node at the specified position in this list
     * @throws IndexOutOfBoundsException if the index is out of range
     *         ({@code index < 0 || index >= size()})
     */
	private Node<T> getNode(int index) { // O(index)
		if (index < 0 || index >= size()) { // O(1)
			throw new IndexOutOfBoundsException("Index is out of bounds"); // O(1)
		}
		Node <T> node = head; // O(1)
		for (int i = 0; i < index; i++) { // O(index): the time complexity depends on the index, not the size of the linked list
			node = node.next; // O(1) 		 not sure if that would qualify it as being O(n) or something in-between O(1) and O(n)
		}
		return node; // O(1)
	}

	@Override
	public void add(int index, T element) { // O(index)
		Node <T> new_node = new Node <T> (element); // O(1)
		Node <T> node = head; // O(1)

		if (index == 0) { // O(1)
			head = new_node; // O(1)
			head.next = node; // O(1)
		}
		else {
			for (int i = 0; i < index - 1; i++) { // O(index)
				node = node.next; // O(1)
			}
			new_node.next = node.next; // O(1)
			node.next = new_node; // O(1)
		}

		n += 1;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(n*3 + 2);
		str.append("[");
		Node<T> currentNode = head;
		while (currentNode.next != null) {
			str.append(currentNode.data);
			str.append(", ");
			currentNode = currentNode.next;
		}
		str.append((T) currentNode.data);
		str.append("]");
		return str.toString();
	}

	@SuppressWarnings("hiding")
	private class Node<T> {
		T data;
		Node<T> next;

		public Node(T data) {
			this.data = data;
		}
	}
	
}