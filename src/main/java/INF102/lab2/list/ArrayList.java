package INF102.lab2.list;

import java.util.Arrays;

public class ArrayList<T> implements List<T> {

	
	public static final int DEFAULT_CAPACITY = 10;
	
	private int n;
	
	private Object elements[];
	
	public ArrayList() {
		elements = new Object[DEFAULT_CAPACITY];
	}
		
	@Override
	public T get(int index) { // O(1)
		if (index < 0 || index >= size()) { // O(1)
			throw new IndexOutOfBoundsException("Index is out of bounds"); // O(1)
		}
		T element = (T) elements[index]; // O(1)
		return element; // O(1)
	}
	
	@Override
	public void add(int index, T element) { // O(n)
		Object newElements[] = new Object[n + 1]; // O(1)
		for (int i = 0; i < index; i++) { // O(n)
			newElements[i] = elements[i]; // O(n)
		}
		newElements[index] = element; // O(1)
		for (int i = index; i < n; i++) { // O(n)
			newElements[i + 1] = elements[i]; // O(n)
		}
		elements = newElements; // O(1)
		n += 1; // O(1)
	}
	
	@Override
	public int size() {
		return n;
	}

	@Override
	public boolean isEmpty() {
		return n == 0;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(n*3 + 2);
		str.append("[");
		for (int i = 0; i < n; i++) {
			str.append((T) elements[i]);
			if (i != n-1)
				str.append(", ");
		}
		str.append("]");
		return str.toString();
	}

}